//version inicial

var express = require('express'),
  app = express(),
  port = process.env.PORT || 3000;

var bodyParser = require('body-parser')
app.use(bodyParser.json())
app.use(function(req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
  next();
})

var requestjson = require('request-json');

var path = require('path');

var urlmovimientosMLab = "https://api.mlab.com/api/1/databases/eballesterosf/collections/movimientos?apiKey=GOLqWa850qO8tsdCUdby6eq9eKPInBkt";

var clienteMLab = requestjson.createClient(urlmovimientosMLab);

var movimientosv2JSON = require('./movimientosv2.json');

app.listen(port);

console.log('todo list RESTful API server started on: ' + port);

app.get('/', (req, res) => res.sendFile(path.join(__dirname, 'index.html')));

app.get('/clientes/:idCliente', (req, res) => res.send('Aquì tiene al cliente: ' + req.params.idCliente));

app.get('/v1/movimientos', (req, res) => res.sendFile(path.join(__dirname, 'movimientosv1.json')));

app.get('/v2/movimientos', (req, res) => res.send(movimientosv2JSON));

app.get('/v2/movimientos/:id', (req, res) => res.send(movimientosv2JSON.filter(o => o.id == req.params.id)));

app.get('/v2/movimientosq', (req, res) => {
  console.log(req.query);
  res.send();
});

app.post('/', (req, res) => res.send('Hola, hemos recibido su petición POST cambiada'));

app.post('/v2/movimientos', (req, res) => {
  let nuevo = req.body;
  nuevo.id = movimientosv2JSON.length + 1;
  movimientosv2JSON.push(nuevo);
  res.send(movimientosv2JSON);
});
